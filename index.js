"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const https = require("https");
const events_1 = require("events");
const fs = require("fs");
const querystring = require("querystring");
const httpsRequest = (data) => {
    return new Promise((resolve, reject) => {
        let body = '';
        let request = https.request({ host: data.host, path: data.path }, result => {
            result.setEncoding('utf8');
            result.on('data', chunk => {
                body += chunk;
            });
            result.on('end', () => {
                resolve({ ok: true, status: result.statusCode, headers: result.headers, body: body });
            });
        });
        request.on('error', err => {
            reject({ ok: false, error: err });
        });
        request.end();
    });
};
class TGNodeBot {
    constructor(bot_apikey, options) {
        this.bot_options = {
            bot_name: 'TGNodeBot',
            debug_mode: false,
            auto_start: true
        };
        if (options) {
            if (options.bot_name)
                this.bot_options.bot_name = options.bot_name;
            if (options.debug_mode) {
                this.bot_options.debug_mode = options.debug_mode;
                this.log('Debug mode enabled. Updates will be logged in the console in JSON.');
            }
            if (options.auto_start == false) {
                this.bot_options.auto_start = false;
            }
        }
        this.emitter = new events_1.EventEmitter();
        this.offset = -1;
        this.bot_apikey = bot_apikey;
        if (this.bot_options.auto_start) {
            this.start();
        }
    }
    log(text) {
        let date = new Date();
        let day = '' + date.getUTCDate();
        let month = '' + date.getUTCMonth() + 1;
        let year = date.getUTCFullYear();
        let hour = '' + date.getUTCHours();
        let minute = '' + date.getUTCMinutes();
        let second = '' + date.getUTCSeconds();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        if (second.length < 2)
            second = '0' + second;
        if (minute.length < 2)
            minute = '0' + minute;
        if (hour.length < 2)
            hour = '0' + hour;
        console.log('[' + day + '/' + month + '/' + year + '] [' + hour + ':' + minute + ':' + second + '] [' + this.bot_options.bot_name + '] ' + text);
        fs.appendFileSync('./logs/telegram_bot.log', '[' + day + '/' + month + '/' + year + '] [' + hour + ':' + minute + ':' + second + '] [' + this.bot_options.bot_name + '] ' + text + '\n');
    }
    getUpdates() {
        let self = this;
        httpsRequest({ host: 'api.telegram.org', path: '/bot' + this.bot_apikey + '/getUpdates?offset=' + this.offset }).then(result => {
            if (result.status === 200) {
                let updates = JSON.parse(result.body).result;
                if (updates.length > 0) {
                    for (let i = 0; i < updates.length; i++) {
                        let update = updates[i];
                        self.offset = update.update_id + 1;
                        let update_type = Object.keys(update)[1];
                        if (self.bot_options.debug_mode) {
                            console.log(JSON.stringify(update));
                        }
                        else {
                            this.log('New update received. (' + update_type + ')');
                        }
                        self.emitter.emit(update_type, update[update_type]);
                    }
                }
                self.getUpdates();
            }
            else {
                this.log('Bad response code received! Retrying in 5 seconds...');
                setTimeout(() => {
                    self.getUpdates();
                }, 5000);
            }
        }).catch(() => {
            this.log('HTTP request failed! Retrying in 5 seconds...');
            setTimeout(() => {
                self.getUpdates();
            }, 5000);
        });
    }
    callMethod(method, args) {
        return new Promise((resolve, reject) => {
            let request_options;
            if (args) {
                Object.keys(args).forEach(arg => {
                    if (typeof args[arg] === 'object') {
                        args[arg] = JSON.stringify(args[arg]);
                    }
                });
                request_options = { host: 'api.telegram.org', path: '/bot' + this.bot_apikey + '/' + method + '?' + querystring.stringify(args) };
            }
            else
                request_options = { host: 'api.telegram.org', path: '/bot' + this.bot_apikey + '/' + method };
            httpsRequest(request_options).then(result => {
                let response = JSON.parse(result.body);
                if (result.ok && result.status === 200) {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            });
        });
    }
    start() {
        return new Promise((resolve) => {
            if (this.bot_options.auto_start == false) {
                this.log('Starting TGNodeBot!');
                this.log('Checking bot..');
                this.callMethod('getMe').then(() => {
                    this.getUpdates();
                    this.log('All good! Started getting updates.');
                }).catch(() => {
                    this.log('Mhh.. The apikey seems not working.');
                });
                resolve();
            }
            else {
                this.log('The bot is already running.');
                resolve();
            }
        });
    }
    on(update_type, update_cb) {
        this.emitter.on(update_type, (update_content) => {
            update_cb(update_content);
        });
    }
    onMessage(update_cb) {
        this.emitter.on('message', (msg) => {
            update_cb(msg);
        });
    }
    onEditedMessage(update_cb) {
        this.emitter.on('edited_message', (msg) => {
            update_cb(msg);
        });
    }
    onChannelPost(update_cb) {
        this.emitter.on('channel_post', (msg) => {
            update_cb(msg);
        });
    }
    onEditedChannelPost(update_cb) {
        this.emitter.on('edited_channel_post', (msg) => {
            update_cb(msg);
        });
    }
    onShippingQuery(update_cb) {
        this.emitter.on('shipping_query', (msg) => {
            update_cb(msg);
        });
    }
    onPreCheckoutQuery(update_cb) {
        this.emitter.on('pre_checkout_query', (msg) => {
            update_cb(msg);
        });
    }
    onPoll(update_cb) {
        this.emitter.on('poll', (msg) => {
            update_cb(msg);
        });
    }
    onPollAnswer(update_cb) {
        this.emitter.on('poll_answer', (msg) => {
            update_cb(msg);
        });
    }
    onCallbackQuery(update_cb) {
        this.emitter.on('callback_query', (msg) => {
            update_cb(msg);
        });
    }
    getMe() {
        return new Promise((resolve, reject) => {
            this.callMethod('getMe').then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    forwardMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('forwardMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendAudio(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAudio', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendDocument(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendDocument', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVideo(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideo', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendAnimation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAnimation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVoice(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVideoNote(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideoNote', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendMediaGroup(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMediaGroup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageLiveLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    stopMessageLiveLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('stopMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVenue(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVenue', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendContact(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendContact', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendPoll(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendChatAction(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendChatAction', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getUserProfilePhotos(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getUserProfilePhotos', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getFile(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    kickChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('kickChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    unbanChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('unbanChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    restrictChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('restrictChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    promoteChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('promoteChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatAdministratorCustomTitle(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatAdministratorCustomTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatPermissions(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPermissions', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    exportChatInviteLink(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('exportChatInviteLink', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteChatPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatTitle(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatDescription(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatDescription', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    pinChatMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('pinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    unpinChatMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('unpinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    leaveChat(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('leaveChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChat(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatAdministrators(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatAdministrators', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatMembersCount(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMembersCount', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteChatStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerCallbackQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerCallbackQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageText(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageText', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageCaption(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageCaption', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageMedia(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageMedia', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageReplyMarkup(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageReplyMarkup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    stopPoll(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('stopPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendSticker(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendSticker', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    uploadStickerFile(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('uploadStickerFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    createNewStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('createNewStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    addStickerToSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('addStickerToSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setStickerPositionInSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setStickerPositionInSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteStickerFromSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteStickerFromSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendInvoice(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendInvoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerShippingQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerShippingQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerPreCheckoutQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerPreCheckoutQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setPassportDataErrors(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setPassportDataErrors', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendGame(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendGame', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setGameScore(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setGameScore', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getGameHighScores(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getGameHighScores', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
}
exports.TGNodeBot = TGNodeBot;
class TGMethods {
    constructor(bot_apikey) {
        this.bot_apikey = bot_apikey;
    }
    callMethod(method, args) {
        return new Promise((resolve, reject) => {
            let request_options;
            if (args)
                request_options = { host: 'api.telegram.org', path: '/bot' + this.bot_apikey + '/' + method + '?' + querystring.stringify(args) };
            else
                request_options = { host: 'api.telegram.org', path: '/bot' + this.bot_apikey + '/' + method };
            httpsRequest(request_options).then(result => {
                let response = JSON.parse(result.body);
                if (result.ok && result.status === 200) {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            });
        });
    }
    getMe() {
        return new Promise((resolve, reject) => {
            this.callMethod('getMe').then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    forwardMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('forwardMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendAudio(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAudio', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendDocument(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendDocument', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVideo(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideo', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendAnimation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAnimation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVoice(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVideoNote(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideoNote', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendMediaGroup(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMediaGroup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageLiveLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    stopMessageLiveLocation(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('stopMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendVenue(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVenue', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendContact(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendContact', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendPoll(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendChatAction(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendChatAction', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getUserProfilePhotos(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getUserProfilePhotos', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getFile(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    kickChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('kickChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    unbanChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('unbanChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    restrictChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('restrictChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    promoteChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('promoteChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatAdministratorCustomTitle(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatAdministratorCustomTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatPermissions(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPermissions', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    exportChatInviteLink(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('exportChatInviteLink', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteChatPhoto(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatTitle(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatDescription(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatDescription', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    pinChatMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('pinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    unpinChatMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('unpinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    leaveChat(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('leaveChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChat(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatAdministrators(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatAdministrators', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatMembersCount(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMembersCount', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getChatMember(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setChatStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteChatStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerCallbackQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerCallbackQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageText(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageText', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageCaption(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageCaption', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageMedia(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageMedia', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    editMessageReplyMarkup(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageReplyMarkup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    stopPoll(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('stopPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteMessage(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendSticker(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendSticker', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    uploadStickerFile(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('uploadStickerFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    createNewStickerSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('createNewStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    addStickerToSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('addStickerToSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setStickerPositionInSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setStickerPositionInSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    deleteStickerFromSet(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteStickerFromSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendInvoice(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendInvoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerShippingQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerShippingQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    answerPreCheckoutQuery(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('answerPreCheckoutQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setPassportDataErrors(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setPassportDataErrors', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    sendGame(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('sendGame', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    setGameScore(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('setGameScore', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    getGameHighScores(args) {
        return new Promise((resolve, reject) => {
            this.callMethod('getGameHighScores', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
}
exports.TGMethods = TGMethods;
