import { TGMethodsTypes, TGTypes, Types }  from './typings/index.d';
import * as https from 'https';
import { EventEmitter } from 'events';
import * as fs from 'fs';
import * as querystring from 'querystring';

const httpsRequest = (data: Types.HTTPSRequestOptions): Promise<Types.HTTPSRequestResult> => {
    return new Promise((resolve, reject) => {
        
        let body = '';
        let request = https.request({host:data.host, path:data.path}, result => {
            result.setEncoding('utf8');
    
            result.on('data', chunk => {
                body += chunk;
            });
    
            result.on('end', () => {
                resolve({ok:true, status:result.statusCode, headers:result.headers, body:body});
            });
        });
    
        request.on('error', err => {
            reject({ok:false, error:err});
        });
    
        request.end();
    });
};

export class TGNodeBot {
    private bot_options: Types.BotOptionsRequired;
    private emitter: EventEmitter;
    private offset: number;
    private bot_apikey: string;

    constructor (bot_apikey: string, options?: Types.BotOptions) {
        this.bot_options = {
            bot_name:'TGNodeBot',
            debug_mode:false,
            auto_start:true
        };
        
        if (options) {
            if (options.bot_name) this.bot_options.bot_name = options.bot_name;
            
            if (options.debug_mode) { 
                this.bot_options.debug_mode = options.debug_mode;
                this.log('Debug mode enabled. Updates will be logged in the console in JSON.');     
            }

            if (options.auto_start == false) {
                this.bot_options.auto_start = false;
            }
        }
        
        this.emitter = new EventEmitter();
        this.offset = -1;
        this.bot_apikey = bot_apikey;

        if (this.bot_options.auto_start) {
            this.start();
        }
    }

    private log(text: string): void {
        let date = new Date();
    
        let day = '' + date.getUTCDate();
        let month = '' + date.getUTCMonth() + 1;
        let year = date.getUTCFullYear();
    
        let hour = '' + date.getUTCHours();
        let minute = '' + date.getUTCMinutes();
        let second = '' + date.getUTCSeconds();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (second.length < 2) second = '0' + second;
        if (minute.length < 2) minute = '0' + minute;
        if (hour.length < 2) hour = '0' + hour;
    
        console.log('['+day+'/'+month+'/'+year+'] ['+hour+':'+minute+':'+second+'] ['+this.bot_options.bot_name+'] '+text);
        fs.appendFileSync('./logs/telegram_bot.log', '['+day+'/'+month+'/'+year+'] ['+hour+':'+minute+':'+second+'] ['+this.bot_options.bot_name+'] '+text+'\n');
	}

    private getUpdates(): void {
        let self = this;
        httpsRequest({host:'api.telegram.org', path:'/bot'+this.bot_apikey+'/getUpdates?offset='+this.offset}).then(result => {
            if (result.status === 200) {
                let updates = JSON.parse(result.body).result;

                if (updates.length > 0) {
                    for (let i = 0; i < updates.length; i++) {
                        let update = updates[i];
                        self.offset = update.update_id + 1;

                        let update_type = Object.keys(update)[1];

                        if (self.bot_options.debug_mode) {
                            console.log(JSON.stringify(update));
                        } else {
                            this.log('New update received. ('+update_type+')');
                        }

                        self.emitter.emit(update_type, update[update_type]);
                    }
                }

                self.getUpdates();
            } else {
                this.log('Bad response code received! Retrying in 5 seconds...');
                setTimeout(() => {
                    self.getUpdates();
                }, 5000);
            }
        }).catch(() => {
            this.log('HTTP request failed! Retrying in 5 seconds...');
            setTimeout(() => {
                self.getUpdates();
            }, 5000);
        });
    }

    public callMethod(method: string, args?: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let request_options: {host: string, path: string};

            if (args) {
                Object.keys(args).forEach(arg => {
                    if (typeof args[arg] === 'object') {
                        args[arg] = JSON.stringify(args[arg]);
                    }
                });
                
                request_options = {host:'api.telegram.org', path:'/bot'+this.bot_apikey+'/'+method+'?'+querystring.stringify(args)};
            } else request_options = {host:'api.telegram.org', path:'/bot'+this.bot_apikey+'/'+method};

            httpsRequest(request_options).then(result => {
                let response = JSON.parse(result.body);

                if (result.ok && result.status === 200) {
                    resolve(response);
                } else {
                    reject(response);
                }
            });
        });
    }

    public start(): Promise<void> {
        return new Promise((resolve) => {
            if (this.bot_options.auto_start == false) {
                this.log('Starting TGNodeBot!');
                this.log('Checking bot..');
                this.callMethod('getMe').then(() => {
                    this.getUpdates();
                    this.log('All good! Started getting updates.');
                }).catch(() => {
                    this.log('Mhh.. The apikey seems not working.')
                });
                resolve();
            } else {
                this.log('The bot is already running.')
                resolve();
            }
        });
    }

    public on(update_type: TGTypes.UpdateType, update_cb: (update_content: object) => any): void {
        this.emitter.on(update_type, (update_content: object) => {
            update_cb(update_content);
        });
    }

    public onMessage(update_cb: (message: TGTypes.Message) => any): void {
        this.emitter.on('message', (msg: TGTypes.Message) => {
            update_cb(msg);
        });
    }

    public onEditedMessage(update_cb: (edited_message: TGTypes.Message) => any): void {
        this.emitter.on('edited_message', (msg: TGTypes.Message) => {
            update_cb(msg);
        });
    }

    public onChannelPost(update_cb: (channel_post: TGTypes.Message) => any): void {
        this.emitter.on('channel_post', (msg: TGTypes.Message) => {
            update_cb(msg);
        });
    }

    public onEditedChannelPost(update_cb: (edited_channel_post: TGTypes.Message) => any): void {
        this.emitter.on('edited_channel_post', (msg: TGTypes.Message) => {
            update_cb(msg);
        });
    }

    public onShippingQuery(update_cb: (shipping_query: TGTypes.ShippingQuery) => any): void {
        this.emitter.on('shipping_query', (msg: TGTypes.ShippingQuery) => {
            update_cb(msg);
        });
    }

    public onPreCheckoutQuery(update_cb: (pre_checkout_query: TGTypes.PreCheckoutQuery) => any): void {
        this.emitter.on('pre_checkout_query', (msg: TGTypes.PreCheckoutQuery) => {
            update_cb(msg);
        });
    }

    public onPoll(update_cb: (poll: TGTypes.Poll) => any): void {
        this.emitter.on('poll', (msg: TGTypes.Poll) => {
            update_cb(msg);
        });
    }

    public onPollAnswer(update_cb: (poll_answer: TGTypes.PollAnswer) => any): void {
        this.emitter.on('poll_answer', (msg: TGTypes.PollAnswer) => {
            update_cb(msg);
        });
    }

    public onCallbackQuery(update_cb: (callback_query: TGTypes.CallbackQuery) => any): void {
        this.emitter.on('callback_query', (msg: TGTypes.CallbackQuery) => {
            update_cb(msg);
        });
    }

    public getMe(): Promise<TGTypes.User> {
        return new Promise((resolve, reject) => {
            this.callMethod('getMe').then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    } 

    public sendMessage(args: TGMethodsTypes.SendMessage): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public forwardMessage(args: TGMethodsTypes.ForwardMessage): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('forwardMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendPhoto(args: TGMethodsTypes.SendPhoto): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendAudio(args: TGMethodsTypes.SendAudio): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAudio', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendDocument(args: TGMethodsTypes.SendDocument): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendDocument', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVideo(args: TGMethodsTypes.SendVideo): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideo', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendAnimation(args: TGMethodsTypes.SendAnimation): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAnimation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVoice(args: TGMethodsTypes.SendVoice): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVideoNote(args: TGMethodsTypes.SendVideoNote): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideoNote', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public sendMediaGroup(args: TGMethodsTypes.SendMediaGroup): Promise<Array<TGTypes.Message>> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMediaGroup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendLocation(args: TGMethodsTypes.SendLocation): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageLiveLocation(args: TGMethodsTypes.EditMessageLiveLocation): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public stopMessageLiveLocation(args: TGMethodsTypes.StopMessageLiveLocation): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('stopMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVenue(args: TGMethodsTypes.SendVenue): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVenue', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendContact(args: TGMethodsTypes.SendContact): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendContact', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendPoll(args: TGMethodsTypes.SendPoll): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendChatAction(args: TGMethodsTypes.SendChatAction): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendChatAction', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getUserProfilePhotos(args: TGMethodsTypes.GetUserProfilePhotos): Promise<TGTypes.UserProfilePhotos> {
        return new Promise((resolve, reject) => {
            this.callMethod('getUserProfilePhotos', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public getFile(args: TGMethodsTypes.GetFile): Promise<TGTypes.File> {
        return new Promise((resolve, reject) => {
            this.callMethod('getFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public kickChatMember(args: TGMethodsTypes.KickChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('kickChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public unbanChatMember(args: TGMethodsTypes.UnbanChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('unbanChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public restrictChatMember(args: TGMethodsTypes.RestrictChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('restrictChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public promoteChatMember(args: TGMethodsTypes.PromoteChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('promoteChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatAdministratorCustomTitle(args: TGMethodsTypes.SetChatAdministratorCustomTitle): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatAdministratorCustomTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatPermissions(args: TGMethodsTypes.SetChatPermissions): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPermissions', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public exportChatInviteLink(args: TGMethodsTypes.ExportChatInviteLink): Promise<string> {
        return new Promise((resolve, reject) => {
            this.callMethod('exportChatInviteLink', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatPhoto(args: TGMethodsTypes.SetChatPhoto): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteChatPhoto(args: TGMethodsTypes.DeleteChatPhoto): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatTitle(args: TGMethodsTypes.SetChatTitle): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatDescription(args: TGMethodsTypes.SetChatDescription): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatDescription', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public pinChatMessage(args: TGMethodsTypes.PinChatMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('pinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public unpinChatMessage(args: TGMethodsTypes.UnpinChatMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('unpinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public leaveChat(args: TGMethodsTypes.LeaveChat): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('leaveChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChat(args: TGMethodsTypes.GetChat): Promise<TGTypes.Chat> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChatAdministrators(args: TGMethodsTypes.GetChatAdministrators): Promise<Array<TGTypes.ChatMember>> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatAdministrators', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChatMembersCount(args: TGMethodsTypes.GetChatMembersCount): Promise<number> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMembersCount', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public getChatMember(args: TGMethodsTypes.GetChatMember): Promise<TGTypes.ChatMember> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
        
    public setChatStickerSet(args: TGMethodsTypes.SetChatStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteChatStickerSet(args: TGMethodsTypes.DeleteChatStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public answerCallbackQuery(args: TGMethodsTypes.AnswerCallbackQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerCallbackQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageText(args: TGMethodsTypes.EditMessageText): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageText', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageCaption(args: TGMethodsTypes.EditMessageCaption): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageCaption', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageMedia(args: TGMethodsTypes.EditMessageMedia): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageMedia', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageReplyMarkup(args: TGMethodsTypes.EditMessageReplyMarkup): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageReplyMarkup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public stopPoll(args: TGMethodsTypes.StopPoll): Promise<TGTypes.Poll> {
        return new Promise((resolve, reject) => {
            this.callMethod('stopPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteMessage(args: TGMethodsTypes.DeleteMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendSticker(args: TGMethodsTypes.SendSticker): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendSticker', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getStickerSet(args: TGMethodsTypes.GetStickerSet): Promise<TGTypes.StickerSet> {
        return new Promise((resolve, reject) => {
            this.callMethod('getStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public uploadStickerFile(args: TGMethodsTypes.UploadStickerFile): Promise<TGTypes.File> {
        return new Promise((resolve, reject) => {
            this.callMethod('uploadStickerFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public createNewStickerSet(args: TGMethodsTypes.CreateNewStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('createNewStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public addStickerToSet(args: TGMethodsTypes.AddStickerToSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('addStickerToSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setStickerPositionInSet(args: TGMethodsTypes.SetStickerPositionInSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setStickerPositionInSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteStickerFromSet(args: TGMethodsTypes.DeleteStickerFromSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteStickerFromSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendInvoice(args: TGMethodsTypes.SendInvoice): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendInvoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public answerShippingQuery(args: TGMethodsTypes.AnswerShippingQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerShippingQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public answerPreCheckoutQuery(args: TGMethodsTypes.AnswerPreCheckoutQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerPreCheckoutQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setPassportDataErrors(args: TGMethodsTypes.SetPassportDataErrors): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setPassportDataErrors', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendGame(args: TGMethodsTypes.SendGame): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendGame', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setGameScore(args: TGMethodsTypes.SetGameScore): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setGameScore', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getGameHighScores(args: TGMethodsTypes.GetGameHighScores): Promise<Array<TGTypes.GameHighScore>> {
        return new Promise((resolve, reject) => {
            this.callMethod('getGameHighScores', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
}

export class TGMethods {
    private bot_apikey: string;

    constructor (bot_apikey: string) {
        this.bot_apikey = bot_apikey;
    }

    public callMethod(method: string, args?: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let request_options: {host: string, path: string};

            if (args) request_options = {host:'api.telegram.org', path:'/bot'+this.bot_apikey+'/'+method+'?'+querystring.stringify(args)};
            else request_options = {host:'api.telegram.org', path:'/bot'+this.bot_apikey+'/'+method};

            httpsRequest(request_options).then(result => {
                let response = JSON.parse(result.body);

                if (result.ok && result.status === 200) {
                    resolve(response);
                } else {
                    reject(response);
                }
            });
        });
    }

    public getMe(): Promise<TGTypes.User> {
        return new Promise((resolve, reject) => {
            this.callMethod('getMe').then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    } 

    public sendMessage(args: TGMethodsTypes.SendMessage): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public forwardMessage(args: TGMethodsTypes.ForwardMessage): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('forwardMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendPhoto(args: TGMethodsTypes.SendPhoto): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendAudio(args: TGMethodsTypes.SendAudio): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAudio', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendDocument(args: TGMethodsTypes.SendDocument): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendDocument', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVideo(args: TGMethodsTypes.SendVideo): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideo', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendAnimation(args: TGMethodsTypes.SendAnimation): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendAnimation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVoice(args: TGMethodsTypes.SendVoice): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVideoNote(args: TGMethodsTypes.SendVideoNote): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVideoNote', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public sendMediaGroup(args: TGMethodsTypes.SendMediaGroup): Promise<Array<TGTypes.Message>> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendMediaGroup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendLocation(args: TGMethodsTypes.SendLocation): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageLiveLocation(args: TGMethodsTypes.EditMessageLiveLocation): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public stopMessageLiveLocation(args: TGMethodsTypes.StopMessageLiveLocation): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('stopMessageLiveLocation', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendVenue(args: TGMethodsTypes.SendVenue): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendVenue', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendContact(args: TGMethodsTypes.SendContact): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendContact', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendPoll(args: TGMethodsTypes.SendPoll): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendChatAction(args: TGMethodsTypes.SendChatAction): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendChatAction', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getUserProfilePhotos(args: TGMethodsTypes.GetUserProfilePhotos): Promise<TGTypes.UserProfilePhotos> {
        return new Promise((resolve, reject) => {
            this.callMethod('getUserProfilePhotos', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public getFile(args: TGMethodsTypes.GetFile): Promise<TGTypes.File> {
        return new Promise((resolve, reject) => {
            this.callMethod('getFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public kickChatMember(args: TGMethodsTypes.KickChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('kickChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public unbanChatMember(args: TGMethodsTypes.UnbanChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('unbanChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public restrictChatMember(args: TGMethodsTypes.RestrictChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('restrictChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public promoteChatMember(args: TGMethodsTypes.PromoteChatMember): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('promoteChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatAdministratorCustomTitle(args: TGMethodsTypes.SetChatAdministratorCustomTitle): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatAdministratorCustomTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatPermissions(args: TGMethodsTypes.SetChatPermissions): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPermissions', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public exportChatInviteLink(args: TGMethodsTypes.ExportChatInviteLink): Promise<string> {
        return new Promise((resolve, reject) => {
            this.callMethod('exportChatInviteLink', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatPhoto(args: TGMethodsTypes.SetChatPhoto): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteChatPhoto(args: TGMethodsTypes.DeleteChatPhoto): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatPhoto', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatTitle(args: TGMethodsTypes.SetChatTitle): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatTitle', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setChatDescription(args: TGMethodsTypes.SetChatDescription): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatDescription', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public pinChatMessage(args: TGMethodsTypes.PinChatMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('pinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public unpinChatMessage(args: TGMethodsTypes.UnpinChatMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('unpinChatMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public leaveChat(args: TGMethodsTypes.LeaveChat): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('leaveChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChat(args: TGMethodsTypes.GetChat): Promise<TGTypes.Chat> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChat', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChatAdministrators(args: TGMethodsTypes.GetChatAdministrators): Promise<Array<TGTypes.ChatMember>> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatAdministrators', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getChatMembersCount(args: TGMethodsTypes.GetChatMembersCount): Promise<number> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMembersCount', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public getChatMember(args: TGMethodsTypes.GetChatMember): Promise<TGTypes.ChatMember> {
        return new Promise((resolve, reject) => {
            this.callMethod('getChatMember', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
        
    public setChatStickerSet(args: TGMethodsTypes.SetChatStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteChatStickerSet(args: TGMethodsTypes.DeleteChatStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteChatStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public answerCallbackQuery(args: TGMethodsTypes.AnswerCallbackQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerCallbackQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageText(args: TGMethodsTypes.EditMessageText): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageText', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageCaption(args: TGMethodsTypes.EditMessageCaption): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageCaption', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageMedia(args: TGMethodsTypes.EditMessageMedia): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageMedia', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public editMessageReplyMarkup(args: TGMethodsTypes.EditMessageReplyMarkup): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('editMessageReplyMarkup', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public stopPoll(args: TGMethodsTypes.StopPoll): Promise<TGTypes.Poll> {
        return new Promise((resolve, reject) => {
            this.callMethod('stopPoll', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteMessage(args: TGMethodsTypes.DeleteMessage): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteMessage', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendSticker(args: TGMethodsTypes.SendSticker): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendSticker', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getStickerSet(args: TGMethodsTypes.GetStickerSet): Promise<TGTypes.StickerSet> {
        return new Promise((resolve, reject) => {
            this.callMethod('getStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public uploadStickerFile(args: TGMethodsTypes.UploadStickerFile): Promise<TGTypes.File> {
        return new Promise((resolve, reject) => {
            this.callMethod('uploadStickerFile', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public createNewStickerSet(args: TGMethodsTypes.CreateNewStickerSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('createNewStickerSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public addStickerToSet(args: TGMethodsTypes.AddStickerToSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('addStickerToSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setStickerPositionInSet(args: TGMethodsTypes.SetStickerPositionInSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setStickerPositionInSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public deleteStickerFromSet(args: TGMethodsTypes.DeleteStickerFromSet): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('deleteStickerFromSet', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendInvoice(args: TGMethodsTypes.SendInvoice): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendInvoice', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
    
    public answerShippingQuery(args: TGMethodsTypes.AnswerShippingQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerShippingQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public answerPreCheckoutQuery(args: TGMethodsTypes.AnswerPreCheckoutQuery): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('answerPreCheckoutQuery', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setPassportDataErrors(args: TGMethodsTypes.SetPassportDataErrors): Promise<true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setPassportDataErrors', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public sendGame(args: TGMethodsTypes.SendGame): Promise<TGTypes.Message> {
        return new Promise((resolve, reject) => {
            this.callMethod('sendGame', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public setGameScore(args: TGMethodsTypes.SetGameScore): Promise<TGTypes.Message | true> {
        return new Promise((resolve, reject) => {
            this.callMethod('setGameScore', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }

    public getGameHighScores(args: TGMethodsTypes.GetGameHighScores): Promise<Array<TGTypes.GameHighScore>> {
        return new Promise((resolve, reject) => {
            this.callMethod('getGameHighScores', args).then(ris => {
                resolve(ris.result);
            }).catch(err => {
                reject(err);
            });
        });
    }
}
