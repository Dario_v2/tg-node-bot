/// <reference types="node" />

import { IncomingHttpHeaders } from "http";

export namespace Types {
    export interface HTTPSRequestOptions {
        host: string,
        path: string
    }

    export interface BotOptionsRequired {
        debug_mode: boolean,
        bot_name: string,
        auto_start: boolean
    }

    export interface BotOptions {
        debug_mode?: boolean,
        bot_name?: string,
        auto_start?: boolean
    }

    export interface HTTPSRequestResult {
        ok: true,
        status: number,
        headers: IncomingHttpHeaders,
        body: string
    }
}

export namespace TGTypes {
    export type UpdateType = 'message' | 'edited_message' | 'channel_post' | 'edited_channel_post' | 'callback_query' | 'shipping_query' | 'pre_checkout_query' | 'poll' | 'poll_answer';
    
    export interface User {
        id: number,
        is_bot: boolean,
        first_name: string,
        last_name?: string,
        username?: string,
        language_code?: string,
        can_join_groups?: boolean,
        can_read_all_group_messages?: boolean,
        supports_inline_queries?: boolean
    }
    
    export interface Chat {
        id: number,
        type: string,
        title?: string,
        username?: string,
        first_name?: string,
        last_name?: string,
        photo?: Chat,
        description?: string,
        invite_link?: string,
        pinned_message?: Message,
        permissions?: ChatPermissions,
        slow_mode_delay?: number,
        sticker_set_name?: string,
        can_set_sticker_set?: boolean
    }
    
    export interface Message {
        message_id: number,
        from?: User,
        date: number,
        chat: Chat,
        forward_from?: User,
        forward_from_chat?: Chat,
        forward_from_message_id?: number,
        forward_signature?: string,
        forward_sender_name?: string,
        forward_date?: number,
        reply_to_message?: Message,
        edit_date?: number,
        media_group_id?: string,
        author_signature?: string,
        text?: string,
        entities?: Array<MessageEntity>,
        caption_entities?: Array<MessageEntity>,
        audio?: Audio,
        document?: TGDocument,
        animation?: TGAnimation,
        game?: Game,
        photo?: Array<PhotoSize>,
        sticker?: Sticker,
        video?: Video,
        video_node?: VideoNote,
        caption?: string,
        contact?: Contact,
        location?: TGLocation,
        venue?: Venue,
        poll?: Poll,
        new_chat_members?: Array<User>,
        left_chat_member?: User,
        new_chat_title?: string,
        new_chat_photo?: Array<PhotoSize>,
        delete_chat_photo?: true,
        group_chat_created?: true,
        supergroup_chat_created?: true,
        channel_chat_created?: true,
        migrate_to_chat_id?: number,
        migrate_from_chat_id?: number,
        pinned_message?: Message,
        invoice?: Invoice,
        successful_payment?: SuccessfulPayment,
        connected_website?: string,
        passport_data?: PassportData,
        reply_markup?: InlineKeyboardMarkup
    }
    
    export interface MessageEntity {
        type: string,
        offset: number,
        length: number,
        url?: string,
        user?: User,
        language?: string
    }
    
    export interface PhotoSize {
        file_id: string,
        file_unique_id: string,
        width: number,
        height: number,
        file_size?: number
    }
    
    export interface Audio {
        file_id: string,
        file_unique_id: string,
        duration: number,
        performer?: string,
        title?: string,
        mime_type?: string,
        file_size?: number,
        thumb?: PhotoSize
    }
    
    export interface TGDocument {
        file_id: string,
        file_unique_id: string,
        thumb?: PhotoSize,
        file_name?: string,
        mime_type?: string,
        file_size?: number
    }
    
    export interface Video {
        file_id: string,
        file_unique_id: string,
        width: number,
        height: number,
        duration: number,
        thumb?: PhotoSize,
        mime_type?: string,
        file_size?: number
    }
    
    export interface TGAnimation {
        file_id: string,
        file_unique_id: string,
        width: number,
        height: number,
        duration: number,
        thumb?: PhotoSize,
        file_name?: string,
        mime_type?: string,
        file_size?: number
    }
    
    export interface Voice {
        file_id: string,
        file_unique_id: string,
        duration: number,
        mime_type?: string,
        file_size?: number
    }
    
    export interface VideoNote {
        file_id: string,
        file_unique_id: string,
        length: number,
        duration: number,
        thumb?: PhotoSize,
        file_size?: number
    }
    
    export interface Contact {
        phone_number: string,
        first_name: string,
        last_name?: string,
        user_id?: number,
        vcard?: string
    }
    
    export interface TGLocation {
        longitude: number
        latitude: number
    }
    
    export interface Venue {
        location: TGLocation,
        title: string,
        address: string,
        foursquare_id?: string,
        foursquare_type?: string
    }
    
    export interface PollOption {
        text: string,
        voter_count: number
    }

    export interface KeyboardButtonPollType {
        type?: string
    }
    
    export interface Poll {
        id: string,
        question: string,
        options: Array<PollOption>,
        total_voter_count: number,
        is_closed: boolean,
        is_anonymous: boolean,
        type: string,
        allows_multiple_answers: boolean,
        correct_option_id?: number
    }
    
    export interface UserProfilePhotos {
        total_count: number
        photos: Array<Array<PhotoSize>>
    }

    export interface PollAnswer {
        poll_id: string,
        user: User,
        option_ids: Array<number>
    }
    
    export interface File {
        file_id: string,
        file_unique_id: string,
        file_size?: number,
        file_path?: string
    }
    
    export interface ReplyKeyboardMarkup {
        keyboard: Array<Array<KeyboardButton>>,
        resize_keyboard?: boolean,
        one_time_keyboard?: boolean,
        selective?: boolean
    }
    
    export interface KeyboardButton {
        text: string,
        request_contact?: boolean,
        request_location?: boolean
        request_poll?: KeyboardButtonPollType
    }
    
    export interface ReplyKeyboardRemove {
        remove_keyboard: true,
        selective?: boolean
    }
    
    
    
    export interface InlineKeyboardMarkup {
        inline_keyboard: Array<Array<InlineKeyboardButton>>
    }
    
    export interface IKB {
        text: string
    }
    
    export interface IKBUrl extends IKB {
        url: string
    }
    
    export interface IKBLoginUrl extends IKB {
        login_url: LoginUrl
    }
    
    export interface IKBCallbackData extends IKB {
        callback_data: string
    }
    
    export interface IKBSwitchInlineQuery extends IKB {
        switch_inline_query: string
    }
    
    export interface IKBSwitchInlineQueryCurrentChat extends IKB {
        switch_inline_query_current_chat: string
    }
    
    export interface IKBCallbackGame extends IKB {
        callback_game: CallbackGame
    }
    
    export interface IKBPay extends IKB {
        pay: boolean
    }
    
    export type InlineKeyboardButton = IKBUrl | IKBLoginUrl | IKBCallbackData | IKBSwitchInlineQuery | IKBSwitchInlineQueryCurrentChat | IKBCallbackGame | IKBPay
    
    
    
    export interface LoginUrl {
        url: string,
        forward_text?: string,
        bot_username?: string,
        request_write_access?: boolean
    }
    
    export interface CallbackQuery {
        id: string,
        from: User,
        message?: Message,
        inline_message_id?: string,
        chat_instance: string,
        data?: string,
        game_short_name?: string
    }
    
    export interface ForceReply {
        force_reply: true,
        selective?: boolean
    }
    
    export interface ChatPhoto {
        small_file_id: string,
        small_file_unique_id: string,
        big_file_id: string,
        big_file_unique_id: string
    }
    
    export interface ChatMember {
        user: User,
        status: 'creator' | 'administrator' | 'member' | 'restricted' | 'left' | 'kicked',
        custom_title?: string,
        until_date?: number,
        can_be_edited?: boolean,
        can_post_messages?: boolean,
        can_edit_messages?: boolean,
        can_delete_messages?: boolean,
        can_restrict_members?: boolean,
        can_promote_members?: boolean,
        can_change_info?: boolean,
        can_invite_users?: boolean,
        can_pin_messages?: boolean,
        is_member?: boolean,
        can_send_messages?: boolean,
        can_send_media_messages?: boolean,
        can_send_polls?: boolean,
        can_send_other_messages?: boolean,
        can_add_web_page_previews?: boolean
    }
    
    export interface ChatPermissions {
        can_send_messages?: boolean,
        can_send_media_messages?: boolean,
        can_send_polls?: boolean,
        can_send_other_messages?: boolean,
        can_add_web_page_previews?: boolean,
        can_change_info?: boolean,
        can_invite_users?: boolean,
        can_pin_messages?: boolean
    }
    
    export interface ResponseParameters {
        migrate_to_chat_id?: number,
        retry_after?: number
    }
    
    export interface InputMediaPhoto {
        type: string,
        media: string,
        caption?: string,
        parse_mode?: string
    }
    
    export interface InputMediaVideo {
        type: string,
        media: string,
        thumb?: InputFile | string
        caption?: string,
        parse_mode?: string,
        width?: number,
        height?: number,
        duration?: number,
        supports_streaming?: boolean
    }
    
    export interface InputMediaAnimation {
        type: string,
        media: string,
        thumb?: InputFile | string
        caption?: string,
        parse_mode?: string,
        width?: number,
        height?: number,
        duration?: number
    }
    
    export interface InputMediaAudio {
        type: string,
        media: string,
        thumb?: InputFile | string
        caption?: string,
        parse_mode?: string,
        duration?: number,
        performer?: string,
        title?: string
    }
    
    export interface InputMediaDocument {
        type: string,
        media: string,
        thumb?: InputFile | string
        caption?: string,
        parse_mode?: string
    }

    type InputMedia = InputMediaAnimation | InputMediaDocument | InputMediaAudio | InputMediaPhoto | InputMediaVideo
    
    export interface InputFile {
        
    }
    
    export interface Sticker {
        file_id: string,
        file_unique_id: string,
        width: number,
        height: number,
        is_animated: boolean,
        thumb?: PhotoSize,
        emoji?: string,
        set_name?: string,
        mask_position?: MaskPosition,
        file_size?: number
    }
    
    export interface StickerSet {
        name: string,
        title: string,
        is_animated: boolean,
        contains_masks: boolean,
        stickers: Array<Sticker>
    }
    
    export interface MaskPosition {
        point: string,
        x_shift: number,
        y_shift: number,
        scale: number
    }
    
    export interface LabeledPrice {
        label: string,
        amount: number
    }
    
    export interface Invoice {
        title: string,
        description: string,
        start_parameter: string,
        currency: string,
        total_amount: number
    }
    
    export interface ShippingAddress {
        country_code: string,
        state: string,
        city: string,
        street_line1: string,
        street_line2: string,
        post_code: string,
    }
    
    export interface OrderInfo {
        name?: string,
        phone_number?: string,
        email?: string,
        shipping_address?: ShippingAddress
    }
    
    export interface ShippingOption {
        id: string,
        title: string,
        prices: Array<LabeledPrice>
    }
    
    export interface SuccessfulPayment {
        currency: string,
        total_amount: number,
        invoice_payload: string,
        shipping_option_id?: string,
        order_info?: OrderInfo,
        telegram_payment_charge_id: string,
        provider_payment_charge_id: string
    }
    
    export interface ShippingQuery {
        id: string,
        from: User,
        invoice_payload: string,
        shipping_address: ShippingAddress
    }
    
    export interface PreCheckoutQuery {
        id: string,
        from: User,
        currency: string,
        total_amount: number,
        invoice_payload: string,
        shipping_option_id?: string,
        order_info?: OrderInfo
    }
    
    export interface PassportData {
        data: Array<EncryptedPassportElement>,
        credentials: EncryptedCredentials
    }
    
    export interface PassportFile {
        file_id: string,
        file_unique_id: string,
        file_size: number,
        file_date: number
    }
    
    export interface EncryptedPassportElement {
        type: string,
        data?: string,
        phone_number?: string,
        email?: string,
        files?: Array<PassportFile>
        front_side?: PassportFile,
        reverse_side?: PassportFile,
        selfie?: PassportFile,
        translation?: Array<PassportFile>,
        hash: string
    }
    
    export interface EncryptedCredentials {
        data: string,
        hash: string,
        secret: string
    }
    
    export interface PassportElementErrorDataField {
        source: string,
        type: string,
        field_name: string,
        soudata_hashrce: string,
        message: string
    }
    
    export interface PassportElementErrorFrontSide {
        source: string,
        type: string,
        file_hash: string,
        message: string
    }
    
    export interface PassportElementErrorReverseSide {
        source: string,
        type: string,
        file_hash: string,
        message: string
    }
    
    export interface PassportElementErrorSelfie {
        source: string,
        type: string,
        file_hash: string,
        message: string
    }
    
    export interface PassportElementErrorFile {
        source: string,
        type: string,
        file_hash: string,
        message: string
    }
    
    export interface PassportElementErrorFiles {
        source: string,
        type: string,
        file_hashes: Array<string>,
        message: string
    }
    
    export interface PassportElementErrorTranslationFile {
        source: string,
        type: string,
        file_hash: string,
        message: string
    }
    
    export interface PassportElementErrorTranslationFiles {
        source: string,
        type: string,
        file_hashes: Array<string>,
        message: string
    }
    
    export interface PassportElementErrorUnspecified {
        source: string,
        type: string,
        element_hash: string,
        message: string
    }
    
    export interface Game {
        title: string,
        description: string,
        photo: PhotoSize[],
        text?: string,
        text_entities?: MessageEntity[],
        animation?: TGAnimation
    }
    
    export interface CallbackGame {
    
    }
    
    export interface GameHighScore {
        position: number,
        user: User,
        score: number
    }
    
    export type PassportElementError = PassportElementErrorDataField | PassportElementErrorFrontSide | PassportElementErrorReverseSide | PassportElementErrorSelfie | PassportElementErrorFile | PassportElementErrorFiles | PassportElementErrorTranslationFile | PassportElementErrorTranslationFiles | PassportElementErrorUnspecified

}

export namespace TGMethodsTypes {
    export interface SendMessage {
        chat_id: number | string
        text: string,
        parse_mode?: string,
        disable_web_page_preview?: boolean,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface ForwardMessage {
        chat_id: number | string,
        from_chat_id: number | string,
        disable_notification?: boolean,
        message_id: number
    }

    export interface SendPhoto {
        chat_id: number | string,
        photo: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendAudio {
        chat_id: number | string,
        audio: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        duration?: number,
        performer?: string,
        title?: string,
        thumb?: TGTypes.InputFile | string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendDocument {
        chat_id: number | string,
        document: TGTypes.InputFile | string,
        thumb?: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendVideo {
        chat_id: number | string,
        video: TGTypes.InputFile | string,
        duration?: number,
        width?: number,
        height?: number,
        thumb?: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        supports_streaming?: boolean
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendAnimation {
        chat_id: number | string,
        animation: TGTypes.InputFile | string,
        duration?: number,
        width?: number,
        height?: number,
        thumb?: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendVoice {
        chat_id: number | string,
        voice: TGTypes.InputFile | string,
        caption?: string,
        parse_mode?: string,
        duration?: number,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendVideoNote {
        chat_id: number | string,
        video_note: TGTypes.InputFile | string,
        duration?: number,
        length?: number,
        thumb?: TGTypes.InputFile | string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendMediaGroup {
        chat_id: number | string,
        media: Array<TGTypes.InputMediaPhoto> & TGTypes.InputMediaVideo
        disable_notification?: boolean,
        reply_to_message_id?: number,
    }

    export interface SendLocation {
        chat_id: number | string,
        latitude: number,
        longitude: number
        live_period?: number,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface EditMessageLiveLocation {
        chat_id?: number | string,
        message_id?: number,
        inline_message_id?: string,
        latitude: number,
        longitude: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface StopMessageLiveLocation {
        chat_id?: number | string,
        message_id?: number,
        inline_message_id?: boolean,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface SendVenue {
        chat_id: number | string,
        latitude: number,
        longitude: number,
        title: string,
        address: string,
        foursquare_id?: string,
        foursquare_type?: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendContact {
        chat_id: number | string,
        phone_number: string,
        first_name: string,
        last_name?: string,
        vcard?: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendPoll {
        chat_id: number | string,
        question: string,
        options: Array<string>,
        is_anonymous?: boolean,
        type?: 'quiz' | 'regular',
        allows_multiple_answers?: boolean,
        correct_option_id?: number,
        is_closed?: boolean,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface SendChatAction {
        chat_id: number | string,
        action: string
    }

    export interface GetUserProfilePhotos {
        user_id: number,
        offset?: number,
        limit?: number
    }

    export interface GetFile {
        file_id: string
    }

    export interface KickChatMember {
        chat_id: number | string,
        user_id: number,
        until_date?: number
    }

    export interface UnbanChatMember {
        chat_id: number | string,
        user_id: number
    }

    export interface RestrictChatMember {
        chat_id: number | string,
        user_id: number,
        permissions: TGTypes.ChatPermissions,
        until_date?: number
    }

    export interface PromoteChatMember {
        chat_id: number | string,
        user_id: number,
        can_change_info?: boolean,
        can_post_messages?: boolean,
        can_edit_messages?: boolean,
        can_delete_messages?: boolean,
        can_invite_users?: boolean,
        can_restrict_members?: boolean,
        can_pin_messages?: boolean,
        can_promote_members?: boolean
    }

    export interface SetChatAdministratorCustomTitle {
        chat_id: number | string,
        user_id: number,
        custom_title: string
    }

    export interface SetChatPermissions {
        chat_id: number | string,
        permissions: TGTypes.ChatPermissions
    }

    export interface ExportChatInviteLink {
        chat_id: number | string
    }

    export interface SetChatPhoto {
        chat_id: number | string,
        photo?: TGTypes.InputFile
    }

    export interface DeleteChatPhoto {
        chat_id: number | string
    }

    export interface SetChatTitle {
        chat_id: number | string,
        title: string
    }

    export interface SetChatDescription {
        chat_id: number | string,
        description?: string
    }

    export interface PinChatMessage {
        chat_id: number | string,
        message_id: number,
        disable_notification?: boolean
    }

    export interface UnpinChatMessage {
        chat_id: number | string
    }

    export interface LeaveChat {
        chat_id: number | string
    }

    export interface GetChat {
        chat_id: number | string
    }

    export interface GetChatAdministrators {
        chat_id: number | string
    }

    export interface GetChatMembersCount {
        chat_id: number | string
    }

    export interface GetChatMember {
        chat_id: number | string,
        user_id: number
    }

    export interface SetChatStickerSet {
        chat_id: number | string,
        sticker_set_name: string
    }

    export interface DeleteChatStickerSet {
        chat_id: number | string
    }

    export interface AnswerCallbackQuery {
        callback_query_id: string,
        text?: string,
        show_alert?: boolean,
        url?: string,
        cache_time?: number
    }

    export interface EditMessageText {
        chat_id: number | string,
        message_id?: number,
        inline_message_id?: string,
        text: string,
        parse_mode?: string,
        disable_web_page_preview?: boolean,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface EditMessageCaption {
        chat_id?: number | string,
        message_id?: number,
        inline_message_id?: string,
        caption?: string
        parse_mode?: string,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface EditMessageMedia {
        chat_id?: number | string,
        message_id?: number,
        inline_message_id?: string,
        media: TGTypes.InputFile,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface EditMessageReplyMarkup {
        chat_id?: number | string,
        message_id?: number,
        inline_message_id?: string,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface StopPoll {
        chat_id: number | string,
        message_id: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup
    }

    export interface DeleteMessage {
        chat_id: number | string,
        message_id: number
    }

    export interface SendSticker {
        chat_id: number | string
        sticker: TGTypes.InputFile | string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.InlineKeyboardMarkup | TGTypes.ReplyKeyboardMarkup | TGTypes.ReplyKeyboardRemove | TGTypes.ForceReply
    }

    export interface GetStickerSet {
        name: string
    }

    export interface UploadStickerFile {
        user_id: number,
        png_sticker: TGTypes.InputFile
    }

    export interface CreateNewStickerSet {
        user_id: number,
        name: string,
        title: string
        png_sticker: TGTypes.InputFile | string,
        emojis: string,
        contains_masks?: boolean,
        mask_position?: TGTypes.MaskPosition
    }

    export interface AddStickerToSet {
        user_id: number,
        name: string,
        png_sticker: TGTypes.InputFile | string,
        emojis: string,
        mask_position?: TGTypes.MaskPosition
    }

    export interface SetStickerPositionInSet {
        sticker: string,
        position: number
    }

    export interface DeleteStickerFromSet {
        sticker: string
    }

    export interface SendInvoice {
        chat_id: number,
        title: string,
        description: string,
        payload: string,
        provider_token: string,
        start_parameter: string,
        currency: string,
        prices: Array<TGTypes.LabeledPrice>,
        provider_data?: string,
        photo_url?: string,
        photo_size?: number,
        photo_width?: number,
        photo_height?: number,
        need_name?: boolean,
        need_phone_number?: boolean,
        need_email?: boolean,
        need_shipping_address?: boolean,
        send_phone_number_to_provider?: boolean,
        send_email_to_provider?: boolean,
        is_flexible?: boolean,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.ReplyKeyboardMarkup
    }

    export interface AnswerShippingQuery {
        shipping_query_id: string,
        ok: boolean,
        shipping_options?: Array<TGTypes.ShippingOption>,
        error_message?: string
    }
    
    export interface AnswerPreCheckoutQuery {
        pre_checkout_query_id: string,
        ok: boolean,
        error_message?: string
    }

    export interface SetPassportDataErrors {
        user_id: number,
        errors: Array<TGTypes.PassportElementError>
    }

    export interface SendGame {
        chat_id: number,
        game_short_name: string,
        disable_notification?: boolean,
        reply_to_message_id?: number,
        reply_markup?: TGTypes.ReplyKeyboardMarkup
    }

    export interface SetGameScore {
        user_id: number,
        score: number,
        force?: boolean,
        disable_edit_message?: boolean,
        chat_id?: number,
        message_id?: number,
        inline_message_id?: string
    }

    export interface GetGameHighScores {
        user_id: number,
        chat_id?: number,
        message_id?: number,
        inline_message_id?: string
    }
}
